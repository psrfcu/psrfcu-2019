module.exports = {
  content: ["./layouts/**/*.html"],
  theme: {
    fontFamily: {
      heading: ['Playfair Display', 'serif']
    },
    extend: {
      colors: {
        psrfcu: '#0E55A1',
        whiteish: '#F5F5F5'
      }
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/typography')(),
    require('@tailwindcss/forms')({
      strategy: 'class',
    }),
  ]
}
