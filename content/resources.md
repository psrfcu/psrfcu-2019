+++
date = 2019-06-13T07:00:00Z
description = "Find online banking links and use our loan calculators."
featured_image = ""
layout = "blocks"
title = "Resources and Tools"
[menu.main]
name = "Resources"
open_in_new_tab = false
pre = ""
url = "/resources/"
weight = 4

[[page_sections]]
backtotop = false
block = "multi-column"
content_markdown = "Find online banking instructions and use our loan calculators."
headline = "Resources and Tools"
num_col_lg = "1"
num_col_md = "1"
num_col_sm = "1"
num_col_xl = "1"

[[page_sections]]
background_choice = "whiteish"
backtotop = false
block = "cta-banner"
headline = ""

[[page_sections.ctas]]
caption = ""
content = ""
headline = ""
image = "/uploads/information.png"
link_text_markdown = "How-To's"
link_url = "#how-tos"
open_in_new_tab = false

[[page_sections.ctas]]
caption = ""
content = ""
headline = ""
image = "/uploads/finance.png"
link_text_markdown = "Mortgage"
link_url = "#mortgage-calculator"
open_in_new_tab = false

[[page_sections.ctas]]
caption = ""
content = ""
headline = ""
image = "/uploads/accounting.png"
link_text_markdown = "Auto Loan"
link_url = "#auto-loan-calculator"
open_in_new_tab = false

[[page_sections]]
heading_markdown = "How-To's"
background_choice = "psrfcu"
text_choice = "white"
block = "hero"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = false
block = "feature"
content_markdown = "You can now take a picture of a check, and remote deposit capture (RDC) on the app on your phone. Make sure you write on the back of the check 'For mobile deposit PSRFCU only' otherwise it may be rejected."
headline = "Deposit a check remotely!"
media_alignment = "Right"
showmobile = true

[page_sections.cta]
enabled = true
link_text_markdown = "Learn More"
link_url = "https://ondemand.cuanswers.com/mobile-app-basics-2/"
open_in_new_tab = false
[page_sections.media]
alt_text = "Remote Deposit Capture"
image = "/uploads/RDCpic.jpg"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = false
block = "feature"
content_markdown = "Do you need your Monthly, Quarterly, or Year-end statements? You can now easily access them in your online account. If you have never done that before, here are some step-by-step instructions."
headline = "E-Statements"
media_alignment = "Left"
showmobile = true

[page_sections.cta]
enabled = true
link_text_markdown = "Learn More"
link_url = "/uploads/2022_e-Statement_Guide.pdf"
open_in_new_tab = true
[page_sections.media]
alt_text = "Go Paperless"
image = "/uploads/paperless.jpg"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = false
block = "feature"
content_markdown = "PSRFCU Members have an option to skip one payment per loan. For step-by-step instructions click the button below. HELOC Loans are NOT eligible"
headline = "Skip a payment!"
media_alignment = "Right"
showmobile = true

[page_sections.cta]
enabled = true
link_text_markdown = "Learn More"
link_url = "/uploads/2022_Skip_a_pay_how_to.pdf"
open_in_new_tab = false
[page_sections.media]
alt_text = "Skip a Payment"
image = "/uploads/dollar.jpg"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = false
block = "feature"
content_markdown = "Community transmission of Covid-19 is occurring in Skagit County. Now is the time to follow Health Officer Recommendations and protect our community.\n\nThe credit union will remain OPEN, but we are limiting contact for the safety of our membership and employees.\n\n**Need a check?** Please call the office, (360) 293-4862, to make an appointment if you require an in-person meeting, or we can have your check ready to slip through our \"drive thru\" door slot.\n\n**Need to make a deposit?**  You can easily do this using our App (log in and click “Move Money”, the remote capture option will guide you through. _Requires an initial 24 hour approval before deposit of your first check_) [**Click here for step by step instructions on using RDC**](/uploads/rdcsteps.jpg)**.** OR Fill out an envelope, supplied in the mailbox to the right of our door, and slip it through the mail slot.  We can email or mail you a receipt. If you require a receipt immediately, call or text us at (360) 610-7140.  (Please feel free to take the pen with you so we don’t spread germs!)\n\nYou can sign up for online banking, apply for loans, request a check or transfer money online.  We have updated our services to include secure online document signatures so you can sign your documents without leaving home.\n\nPSRFCU also offers banking by phone at 1-800-860-5704.  When prompted enter the 3-digit PIN 948. You will be asked for your 4-digit account number and your personal PIN, which is defaulted to the last 4 numbers of your SSN. You will then be prompted to change your PIN, be sure to keep this handy for all of your banking sessions.\n\nPlease use the links below and if you have any questions feel free to call or email us at info@psrfcu.org .\n\n* Sign up online to access all of your account needs:  [**Access your account HERE**](https://obc.itsme247.com/948/)\n* Apply for auto, personal or HELOC loans: [**Apply for a loan HERE**](https://obc.itsme247.com/948/)\n* Apply for a mortgage loan:  [**Apply for a mortgage HERE**](https://www.consolidatedccu.com/mortgage/mortgage.html?a=PUGETSOUNDREFINERYFCU)\n* Questions about our current account services:  [**View current account services HERE**](https://psrfcu.org/account-services/)\n* Important updates regarding PSRFCU and our members:  [**Find important updates HERE**](https://psrfcu.org/news/)"
headline = "Access to your accounts and all of your lending needs at your fingertips."
media_alignment = "Right"
showmobile = true

[page_sections.cta]
enabled = false
link_text_markdown = ""
link_url = ""
open_in_new_tab = false
[page_sections.media]
alt_text = ""
image = "/uploads/hands-2.jpg"

[[page_sections]]
heading_markdown = "Calculators"
background_choice = "psrfcu"
text_choice = "white"
block = "hero"

[[page_sections]]
heading = "Mortgage Calculator"
content_markdown = "Estimate your monthly payment, total cost of mortgage and total interest paid."
calc_id = "5d02c7c30c34ed002687bf93"
block = "calc"

[[page_sections]]
heading = "Auto Loan Calculator"
content_markdown = "Estimate your monthly payment for an Auto Loan."
calc_id = "5d02c8bd0c34ed002687bf94"
block = "calc"
+++
