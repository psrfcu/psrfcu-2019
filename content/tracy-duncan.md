+++
date = 2019-11-16T08:00:00Z
description = "Meet our employee, Tracy Duncan"
layout = "blocks"
title = "Tracy Duncan"
[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = false
block = "feature"
content_markdown = "I am a Realtor with Keller Williams and on the Board of the Children’s Museum of Skagit County. I have lived in the Skagit Valley since 2012, prior to that I lived in Snohomish County for 21 years where I raised my two kids, Stephen & Shelby. I worked at schools in Marysville and Conway and volunteered many hours for Little League. My hobbies are traveling, fishing, cooking, and spending time with my family and friends. I love living in the Pacific Northwest, it is a beautiful place to call home!"
headline = "Tracy Duncan"
media_alignment = "Left"
showmobile = true

[page_sections.cta]
enabled = false
link_text = ""
link_url = ""
open_in_new_tab = false
[page_sections.media]
alt_text = ""
image = "/uploads/tracy2020.jpg"
+++
