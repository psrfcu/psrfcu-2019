+++
_hide_content = true
draft = false
title = "Account Services"
description = "Find our account services. Deposit accounts, online banking, rates and fees."
date = 2019-06-05T05:00:00.000Z
aliases = [
  "/services/home-banking/",
  "/services/other-services/",
  "/services/deposit-accounts/"
]
featured_image = ""
layout = "blocks"
lastmod = "2022-03-30T17:36:53.656Z"

[menu.main]
name = "Services"
open_in_new_tab = false
pre = ""
weight = 2

[[page_sections]]
backtotop = false
block = "multi-column" 
content_markdown = "We have many services for you to choose from."
headline = "Account Services"
num_col_lg = "1"
num_col_md = "1"
num_col_sm = "1"
num_col_xl = "1"

[[page_sections]]
background_choice = "whiteish"
backtotop = false
block = "cta-banner"
headline = ""

  [[page_sections.ctas]]
  caption = ""
  content_markdown = ""
  headline = "Deposit Accounts"
  image = "/uploads/piggy-bank.png"
  link_text_markdown = "More Information"
  link_url = "#deposit-accounts"
  open_in_new_tab = false

  [[page_sections.ctas]]
  caption = ""
  content_markdown = ""
  headline = "Online and Mobile Banking"
  image = "/uploads/scrolling.png"
  link_text_markdown = "More Information"
  link_url = "#online-and-mobile-banking"
  open_in_new_tab = false

  [[page_sections.ctas]]
  caption = ""
  content_markdown = ""
  headline = "Additional Services & Fees"
  image = "/uploads/handshake.png"
  link_text_markdown = "More Information"
  link_url = "#additional-services"
  open_in_new_tab = false

[[page_sections]]
background_choice = "psrfcu"
block = "hero"
heading_markdown = "Deposit Accounts"
text_choice = "white"

[[page_sections]]
heading = "Account Types"
description_markdown = ""
footnotes_markdown = ""
block = "quicklinks"

  [[page_sections.link]]
  link_text = "Share (Savings) Account"
  link_url = "#share-savings-account"

  [[page_sections.link]]
  link_text = "Early Super Savers Account"
  link_url = "#early-super-savers-account"

  [[page_sections.link]]
  link_text = "Educational Savings Account"
  link_url = "#educational-savings"

  [[page_sections.link]]
  link_text = "Christmas Club Account"
  link_url = "#christmas-club-account"

  [[page_sections.link]]
  link_text = "Certificate of Deposit (CD)"
  link_url = "#certificate-of-deposit-cd"

[[page_sections]]
heading = "Rates and Fees"
description_markdown = ""
footnotes_markdown = ""
block = "quicklinks"

  [[page_sections.link]]
  link_text = "Savings Rates"
  link_url = "#dividend-savings-rates"

  [[page_sections.link]]
  link_text = "Current Fee Schedule"
  link_url = "#current-fees"

[[page_sections]]
backtotop = true
block = "multi-column"
content_markdown = """
Puget Sound Refinery Federal Credit Union offers many deposit account options to help you save and plan for your future.

All of our deposit accounts are Federally Insured by the NCUA up to $250,000.00, which means that your money is safe with us!

### SHARE (SAVINGS) ACCOUNT

This is your membership, which has a $50.00 minimum balance requirement. You may deposit and withdraw at any time. You can also set up payroll deduction or direct deposit with your employer HF Sinclair, or any other financial institution. This is also an interest-bearing account with dividends posted quarterly, as determined by the Board of Directors.

Call us today for information on how to set up automatic ACH deposits to your credit union account!

Minors have a $5.00 minimum balance requirement for membership. You will need an image/copy of their birth certificate and Social Security card which can be uploaded online, emailed, or sent into the Credit Union to finalize the membership application.

### EARLY SUPER SAVERS ACCOUNT

When a minor’s Super Saver account has a $500 deposit, we will pay 1.00% above the Prime Rate to get them off to a great start to be savers for life! This rate is tied to the current Prime Rate plus 1%. Talk to your kids about [financial responsibility](https://nwcua.org/wp-content/uploads/2019/08/BTS-Fin.-Edu.jpeg).  Call now for details.

### CHRISTMAS CLUB ACCOUNT

Open a Christmas Club accounts with $25 and a $25 minimum reoccurring automatic deposit (ACH) to grow your account.  Funds will gain 3.5% interest over the year and are accessible beginning November 1st through December 31st.  

Click on [It’s Me 24/7](https://obc.itsme247.com/948/) and go to New Accounts.  Select “More Share Info” and then “Open” under Christmas Club, you’ll transfer $25 to open the account and then set up a reoccurring monthly deposit of at least $25 under the Pay & Transfer tab at the top of the page.  You can also set up a direct deposit from your payroll.  [Ask us how.](/contact/)

If you run into a need to draw from this account you can access the funds twice a year for a $10 fee each time."""
headline = "Deposit Account Details"
num_col_lg = "2"
num_col_md = "2"
num_col_sm = "2"
num_col_xl = "2"

[[page_sections]]
heading = "Dividend & Savings Rates"
url = "https://docs.google.com/spreadsheets/d/15jDU24F_3RQxL0Yul21lK9sLxUKu2R4BYPRWlqETQhE/export?format=csv"
description_markdown = ""
footnotes_markdown = """&#42;&nbsp;Youth Super Saver accounts are limited to a $500 min/max balance and earn 1.00% above current Prime Rate on $500. This rate is tied to the current Prime Rate plus 1%.If the account were to drop below $500, it would revert to the Youth Share account and rate.<br>&#42;&#42;&nbsp;CD rate changes apply to new money. Ask us about our CD rates as they are always in motion. There is a 180-day interest earned early withdrawal penalty to close a CD prior to maturity. <br>&#42;&#42;&#42;&nbsp;A $25.00 monthly transfer required from an outside financial institution."""
block = "table-csv"

[[page_sections]]
background_choice = "psrfcu"
block = "hero"
heading_markdown = "Online and Mobile Banking"
text_choice = "white"

[[page_sections]]
backtotop = true
block = "multi-column"
content_markdown = """
Enroll now at **It’s Me 24/7**, or stop by the Credit Union for help.

[https://obc.itsme247.com/948/](https://obc.itsme247.com/948/ "https://obc.itsme247.com/948/")

PSRFCU offers Online Banking to give you more access and control over your accounts 24 hours a day, 7 days a week. Manage your money from any Internet-enabled computer when it’s convenient for you.

Online Banking allows you to:

* Check balances on your accounts
* Transfer funds between your accounts
* Remote Deposit Capture
* Access and search transaction history
* Print member savings and loan statements
* View pending transactions
* Review your PSRFCU loans – including Collateralized, Share Secured and Signature
* Export account information to use with other computer software
* Open additional accounts – PSRFCU members can open additional savings accounts online.
* Minors need an image of their birth certificate and Social Security card, and new members over 18 need a copy of their driver’s license or ID card.
* Request new loans by filling out an online loan application. You can even upload images of the title/registration of the 3rd party you are purchasing from and your most recent pay stub to complete the process
* And much more to come!

**Itunes and Google Apps** are here! Login now to try our new fingerprint, voice recognition, and facial recognition access. In the app click the padlock icon to sign in to your account. To learn more about how to log in and use the mobile app there is a [helpful video](https://ondemand.cuanswers.com/mobile-app-basics/ "https://ondemand.cuanswers.com/mobile-app-basics/")."""
headline = "Online and Mobile Banking Details"
num_col_lg = ""
num_col_md = ""
num_col_sm = ""
num_col_xl = ""

[[page_sections]]
background_choice = "psrfcu" 
block = "hero"
heading_markdown = "Additional Services and Fees"
text_choice = "white"

[[page_sections]]
backtotop = true
block = "multi-column"
content_markdown = """
Along with savings and loan services, Puget Sound Refinery Federal Credit Union offers a number of other services to its members. These services are simply meant to help enhance your benefits as a member of the credit union and they are available to you either for free or at a discounted cost.

Members can take advantage of the following additional services at no cost:

* Fax
* Notary
* Copies
* Take member’s checks to guard gate for pick-up
* Local bank deposits done same day if requested before 12:30pm
* Shredding services"""
headline = "Additional Services"
num_col_lg = "1"
num_col_md = "1"
num_col_sm = "1"
num_col_xl = "1"

[[page_sections]]
heading = "Current Fees"
description_markdown = "**PSRFCU Fee Schedule**"
url = "https://docs.google.com/spreadsheets/d/1M1UA4Y92OnTQxBfd_o19YQQrmEBlLCVFlIp5rYXKZto/export?format=csv"
footnotes_markdown = """
There is a 180-day interest earned early withdrawal penalty to close a CD prior to maturity. International Fees are passed on to the member. Fees vary depending on location. 

&#42; NSF returns will be paused until noon each day. If we receive the money to cover the NSF before noon it will not be returned, and there will not be a fee."""
block = "table-csv"
+++
