+++
date = 2019-07-22T07:00:00.000Z
description_markdown = "Meet our current Board Members."
layout = "blocks"
title = "Board Members"
lastmod = "2022-03-30T17:36:53.650Z"

[[page_sections]]
heading = "Board Members"
description_markdown = "Meet our Board Members."
footnotes_markdown = ""
block = "quicklinks"

  [[page_sections.link]]
  link_text = "Tim Norman"
  link_url = "#tim-norman"

  [[page_sections.link]]
  link_text = "Donna Erlandson"
  link_url = "#donna-erlandson"

  [[page_sections.link]]
  link_text = "Kari Harlow"
  link_url = "#kari-harlow"

  [[page_sections.link]]
  link_text = "Dean Iverson"
  link_url = "#dean-iverson"

  [[page_sections.link]]
  link_text = "New Board Member"
  link_url = "#newboardmember"

  [[page_sections.link]]
  link_text = "Larry Miner"
  link_url = "#larry-miner"

  [[page_sections.link]]
  link_text = "Terry Skrabut"
  link_url = "#terry-skrabut"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = true
block = "feature"
content_markdown = """Tim grew up in Bakersfield, California until the age of 15 when he moved to Yuma, Arizona where he graduated from Kofa Highschool in May 2000. He joined the Navy in October 2000 as an Air Traffic Controller and retired from active duty in October 2024 as a Senior Chief Petty Officer. 

Tim currently works as a Refinery Accounting Analyst for HF Sinclair and resides in Oak Harbor. Tim has a Bachelor’s degree from Columbia College and is looking forward to pursuing a Master’s in Finance.

Tim enjoys golf, finance, hiking/camping, and brewing beer. He also takes pleasure in spending time with his family consisting of his wife Kelly, two Labradors (Valentino and Nova), and daughter Lilly who is a Senior at Oak Harbor High School. Lilly will be attending University of Oregon in the fall of 2025."""
headline = "Tim Norman"
media_alignment = "Left"
showmobile = true

  [page_sections.cta]
  enabled = false
  link_text_markdown = ""
  link_url = ""
  open_in_new_tab = false

  [page_sections.media]
  alt_text = ""
  image = "/uploads/tim.jpg"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = true
block = "feature"
content_markdown = """
I have been on the PSRFCU Board of Directors for 8 years.  I am currently the Board Secretary.  I have been a member of the Credit Union for over 50 years, along with my parents, siblings, spouse (Doug), my children and grandchildren.  I have lived in Anacortes my entire life, graduating from Anacortes High School.  

At this time, I am the President of the Salvation Army Board and an Advisor for the Kiwanis Aktion Club.  We follow our daughter, Sonja, in running marathons and participating in Special Olympics.  My passion is supporting people with disabilities on the job, at school and at home.  I also work as a Destination Relocation Consultant for people who are new to the United States and/or our area.  

I love vacationing with Doug anywhere that is warm or on a cruise, and being with my adorable grandchildren Emma (11) and Liam (9)."""
headline = "Donna Erlandson"
media_alignment = "Left"
showmobile = true

  [page_sections.cta]
  enabled = false
  link_text_markdown = ""
  link_url = ""
  open_in_new_tab = false

  [page_sections.media]
  alt_text = "Image of the board member."
  image = "/uploads/Donnapic.jpg"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = true
block = "feature"
content_markdown = """
I have lived in the Anacortes area my whole life.  I have been a credit union member since I was 16 years old.  

I worked for the Anacortes School District for 37 1/2 years and was President of SEIU-120 for 18 years.  I have been employed at Sebos Henery Hardware for 18 years.  

I enjoy hiking and vegetable gardening and bike riding."""
headline = "Kari Harlow"
media_alignment = "Left"
showmobile = true

  [page_sections.cta]
  enabled = false
  link_text_markdown = ""
  link_url = ""
  open_in_new_tab = false

  [page_sections.media]
  alt_text = ""
  image = "/uploads/Karipic2.jpg"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = true
block = "feature"
content_markdown = """
Dean Iverson was born and raised here in Anacortes, and currently lives just outside of town with his better half an 10 month old son.

In his younger days he worked shutdowns a the local refineries, and now works as a journeyman electrician.  In his off time he enjoys spending time with his little family, working in their garden, fishing and crabbing in our local waters, and listening to and playing music.

He has been a member of the credit union since birth, and is looking forward to contributing to the organization."""
headline = "Dean Iverson"
media_alignment = "Left"
showmobile = true

  [page_sections.cta]
  enabled = false
  link_text_markdown = ""
  link_url = ""
  open_in_new_tab = false

  [page_sections.media]
  alt_text = ""
  image = "/uploads/Dean2019pic.JPG"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = true
block = "feature"
content_markdown = """
I grew up in the Yakima Valley and after high school attended JM Perry Technical Institute.  I graduated with an Instrumentation Technology degree.  I moved to the bay area in California for three years before moving to Skagit Valley.

I went to work for Texaco in 1990 and joined the credit union board in 1997.  I was the credit union Secretary until elected in 2005 to replace Bob Mainard as President when he retired.

I currently live on 5 acres near Lyman with my wife Michelle, three border collies, two horses, and a cat.  When not working around the property I enjoy working on cars and going drag racing.

It has been very rewarding to see how the credit union has grown from a simple savings and lending institution to being able to provide most of the services of a large bank while retaining our personal family atmosphere."""
headline = "Larry Miner"
media_alignment = "Left"
showmobile = true

  [page_sections.cta]
  enabled = false
  link_text_markdown = ""
  link_url = ""
  open_in_new_tab = false

  [page_sections.media]
  alt_text = ""
  image = "/uploads/logofinal2.jpg"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
backtotop = true
block = "feature"
content_markdown = """
Terry has been a volunteer for about 2 years on the Supervisory Committee.  He is currently employed by HF Sinclair as an Operator for the past 17 years.

Terry is married to Sandy Skrabut and resides in Bow - East.  They have two kids, TJ (27) and Morgan (22), who both currently live in Bellingham, Washington.

To contact the Supervisory Committee please mail communication to:

PSRFCU, Supervisory Committee

PO Box 1923

Anacortes, WA 98221"""
headline = "Terry Skrabut"
media_alignment = "Left"
showmobile = true

  [page_sections.cta]
  enabled = false
  link_text_markdown = ""
  link_url = ""
  open_in_new_tab = false

  [page_sections.media]
  alt_text = ""
  image = "/uploads/Terry2019b.jpg"
+++
