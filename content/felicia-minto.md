+++
date = 2019-07-29T07:00:00.000Z
description = "Meet our employee, Felicia Minto"
layout = "blocks"
title = "Felicia Minto"
lastmod = "2022-05-27T02:40:16.332Z"

[[page_sections]]
background_choice = "whiteish"
background_image = ""
block = "feature"
content_markdown = "I relocated to the Pacific Northwest by way of Northern California in 2006. I have a Master's Degree from USC in Social Work, and currently volunteer with Skagit County Search & Rescue. I also have a background in bookkeeping and nonprofit management. On my off days, I work on stained glass or my non-profit (Alpine Conservancy). My favorite thing to do is spend time outdoors with my 3 boys, Andrew, Kanyon, and Rowan."
headline = "Felicia Minto"
media_alignment = "Left"
showmobile = true

  [page_sections.cta]
  enabled = false
  link_text_markdown = ""
  link_url = ""
  open_in_new_tab = false

  [page_sections.media]
  alt_text = ""
  image = "/uploads/2022_Felicia_pic.png"
+++
