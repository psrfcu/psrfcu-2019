+++
_hide_content = true
draft = false
title = "Puget Sound Refinery Federal Credit Union Homepage"
date = 2019-08-23T22:20:00.000Z
description = "Find out what PSRFCU is all about, keep up with our current news, and check out vehicles for sale."
featured_image = "/uploads/logofinal2.jpg"
layout = "blocks"
lastmod = "2022-07-25T06:36:39.330Z"

[[page_sections]]
block = "announce-csv"
background_choice = "psrfcu"
text_choice = "white"
url = "https://docs.google.com/spreadsheets/d/1YP2Nn_dU8Ns2QaIyJzBw4QXgCCsCbkIN9Uo1vpW6bhM/export?format=csv"

[[page_sections]]
autoscroll = true
block = "slider"
dots = false
speed = "800"

# [[page_sections.slide]]
# heading = "ANNUAL MEETING!"
# subheading_markdown = "MAY 18th 2023"
# description_markdown = "6:00pm - 8:00pm at the Mt. Baker Building"
# image = "/uploads/age-barros-rBPOfVqROzY-unsplash.jpg"
# link_text_markdown = "Register Now"
# link_url = "http://events.constantcontact.com/register/event?llr=ilzrkrebb&oeidk=a07ejqjcna6dc71d157"
# open_in_new_tab = false
# text_choice = "white"

#  [[page_sections.slide]]
#  heading = "Skip A Loan Payment!"
#  subheading_markdown = "Put a little Jingle in Your Holiday"
#  description_markdown = "1 Skip Per Loan"
#  image = "/uploads/Happy-Holiday-2010.webp"
#  link_text_markdown = "More Details"
#  link_url = "/resources/#how-tos"
#  open_in_new_tab = false
#  text_choice = "white"

  [[page_sections.slide]]
  heading = "Deposit From Home"
  subheading_markdown = "Use Our App"
  description_markdown = "24 Hour First-Time Approval."
  image = "/uploads/mobileapp_test.jpg"
  link_text_markdown = "App How-To"
  link_url = "https://ondemand.cuanswers.com/mobile-app-basics/"
  open_in_new_tab = true
  text_choice = "white"

  [[page_sections.slide]]
  heading = "Earn 8.50% with a Youth Super Saver Account!"
  subheading_markdown = "$500 Minimum Balance"
  description_markdown = ""
  image = "/uploads/vahid-moeini-jazani-tfzl5irgftk-unsplash.jpg"
  link_text_markdown = "Learn more"
  link_url = "https://psrfcu.org/account-services/#deposit-accounts"
  open_in_new_tab = true
  text_choice = "white"

  [[page_sections.slide]]
  heading = "Mortgages & HELOC"
  subheading_markdown = "We now offer both!"
  description_markdown = ""
  image = "/uploads/PSRFCU_Refinery_Background_10.20.21-01_tide_calendar.png"
  link_text_markdown = "Apply Here"
  link_url = "/lending/#home-loans"
  open_in_new_tab = true
  text_choice = "white"

  [[page_sections.slide]]
  heading = "New Member Incentive"
  subheading_markdown = "Earn up to $50 for bringing us an eligible member."
  description_markdown = ""
  image = "/uploads/colin-watts-zymeaawirnq-unsplash.jpg"
  link_text_markdown = "Learn more"
  link_url = "/membership"
  open_in_new_tab = false
  text_choice = "white"
  
  [[page_sections.slide]]
  heading = "Christmas Club Accounts"
  subheading_markdown = "Save Now For The Holidays!"
  description_markdown = "Earn 3.5% on up to $10,000 starting 1/1/2024"
  image = "/uploads/freestocks-k-rp0v0xwwu-unsplash.jpg"
  link_text_markdown = "More Details"
  link_url = "/account-services/#christmas-club-account"
  open_in_new_tab = true
  text_choice = "black"
  
[[page_sections]]
block = "announce"
background_choice = "white"
text_choice = "psrfcu"
content_markdown = "If your debit card isn't working due to fraud prevention, call 888-918-7313 (available 24/7)."

[[page_sections]]
headline = " "
background_choice = "whiteish"
backtotop = false
block = "cta-banner"

  [[page_sections.ctas]]
  headline = "RATES"
  content_markdown = "Rates and Services"
  image = "/uploads/money.png"
  caption = "Black and white money clip art"
  link_text_markdown = "Learn More"
  link_url = "/lending/"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "FEES"
  content_markdown = "Current Fee Schedule"
  image = "/uploads/money1.png"
  caption = "Current Fee Schedule"
  link_text_markdown = "Current Fees"
  link_url = "https://psrfcu.org/account-services/#current-fees"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "CONTACT"
  content_markdown = "We are Here for You"
  image = "/uploads/conversation.png"
  caption = "Contact Us"
  link_text_markdown = "Contact Us Here"
  link_url = "/contact/"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "LOG IN"
  content_markdown = "Access Your Accounts"
  image = "/uploads/scrolling.png"
  caption = "Online Banking Access"
  link_text_markdown = "Login Now"
  link_url = "https://obc.itsme247.com/948/"
  open_in_new_tab = true

#  [[page_sections]]
#  headline = "2024 Annual Member Appreciation"
#  content_markdown = """
#
#  Come by the Credit Union on August 21st from 11am-2pm for a free boxed lunch and some free PSRFCU goodies for your vehicle! We #have a PSRFCU sun visor, car cup coasters, and a free car wash at Blue Cow for the first 100 members. Bring a friend who is #qualified to be a member and we will give them their $50 deposit!
# Please register with the link down below, or email us at info@psrfcu.org, and include the total number of people you are bringing.
# https://lp.constantcontactpages.com/ev/reg/4ngmecm
#
#
# block = "one-column"

[[page_sections]]
headline = "Now Offering Free Appraisals"
content_markdown = "Unlock your dream home with a free mortgage appraisal* with any home purchase. *Appraisal value up to $800, credited after loan closing. Not valid with any other promotions.  See CU for details."
background_choice = "whiteish"
background_image = ""
media_alignment = "Left"
showmobile = true
backtotop = false
block = "feature"

  [page_sections.cta]
  link_text_markdown = "Learn More"
  link_url = "/uploads/CCCU_appraisals.pdf"
  enabled = true
  open_in_new_tab = true

  [page_sections.media]
  alt_text = "Flyer Image"
  image = "/uploads/CCCU_appraisals_001.jpg"

# [[page_sections]]
# headline = "Meet Our Scholarship Winners!"
# content_markdown = "Tori comes from a hard-working Anacortes family, with two brothers already in college. Tori will be attending WSU in the fall and majoring in business and economics. Tori would like to be a CEP of her own company one day."
# background_choice = "whiteish"
# background_image = ""
# media_alignment = "Left"
# showmobile = true
# backtotop = false
# block = "feature"
# 
#   [page_sections.media]
#   alt_text = "Tori"
#   image = "/uploads/Tori.jpg"
# 
#   [[page_sections]]
# headline = "Meet Our Scholarship Winners!"
# content_markdown = "Landen was born and raised in Anacortes. In high school Landen lost both his dad and grandfather to cancer. Landen participated heavily in sports to help get him through. Landen will be attending Bellingham Technical College for the Instrumentations/Control Program, so he can eventually get a position at one of the local refineries."
# background_choice = "whiteish"
# background_image = ""
# media_alignment = "Left"
# showmobile = true
# backtotop = false
# block = "feature"
# 
#   [page_sections.media]
#   alt_text = "Landen"
#   image = "/uploads/Landon.jpg"

[[page_sections]]
headline = "Your Money is Safe With Us!"
content_markdown = "Concerned about the recent FDIC Headlines? Read more about how your money is safe with us."
background_choice = "whiteish"
background_image = ""
media_alignment = "Left"
showmobile = true
backtotop = false
block = "feature"

  [page_sections.cta]
  link_text_markdown = "Learn More"
  link_url = "/uploads/2023 safe and secure.pdf"
  enabled = true
  open_in_new_tab = true

  [page_sections.media]
  alt_text = "Your Money is Safe with Us!"
  image = "/uploads/moneysafe.jpg"

[[page_sections]]
headline = "Switch your Direct Deposit to a Better Than Free E-Checking Account!"
background_choice = "whiteish"
background_image = ""
media_alignment = "Right"
showmobile = true
backtotop = false
block = "feature"

  [page_sections.cta]
  link_text_markdown = "Sign up for E-Checking"
  link_url = "https://www.surveymonkey.com/r/828S5L3"
  enabled = true
  open_in_new_tab = true

  [page_sections.media]
  alt_text = "Mt. Baker and Anacortes refineries in forefront"
  image = "/uploads/bakerrefineries.jpg"

# [[page_sections]]
# heading_markdown = "[Recent News](/news)"
# background_choice = "gray"
# text_choice = "white"
# block = "hero"
# 
# [[page_sections]]
# post_type = "news"
# block = "posts"

[[page_sections]]
heading_markdown = "For Sale"
background_choice = "gray"
text_choice = "white"
block = "hero"

[[page_sections]]
block = "for-sale"
url = "https://docs.google.com/spreadsheets/d/1PUux6rznjWqBqbaFCCPWNzyQTWeCoFR83sRdo8nXrbY/export?format=csv"
+++
