+++
_hide_content = true
title = "About"
description = "Hours of operation, holiday closures, read about the backround of the credit union."
date = 2019-06-05T05:00:00.000Z
aliases = [ "/about-us/" ]
featured_image = ""
layout = "blocks"
lastmod = "2022-05-27T02:39:16.078Z"

[menu.main]
open_in_new_tab = false
pre = ""
url = "/about/"
weight = 6

[[page_sections]]
headline = "Puget Sound Refinery FCU"
content_markdown = """
**Hours of Operation:**

Monday through Friday 
9:00 AM to 5:00 PM  
After hours and weekends by appointment only.
*Closed 1:00 PM to 2:00 PM for lunch*​

**Holiday Schedule:**

Unless otherwise noted, the credit union will be closed on the following holidays:

* New Year’s Day
* President’s Day
* Good Friday
* Memorial Day
* Juneteenth
* Independence Day
* Labor Day
* Columbus Day
* Thanksgiving and the Friday after
* Christmas Eve (closing at noon)
* Christmas Day
* New Year’s Eve (close at noon)"""
num_col_sm = "1"
num_col_md = "1"
num_col_lg = "1"
num_col_xl = "1"
backtotop = false
block = "multi-column"

[[page_sections]]
headline = "Our Staff"
background_choice = "whiteish"
backtotop = true
block = "cta-banner"

  [[page_sections.ctas]]
  headline = "Darene Follett"
  content_markdown = ""
  image = "/uploads/Darene 2019b.jpg"
  caption = ""
  link_text_markdown = "Meet&nbsp;Darene"
  link_url = "/darene-follett/"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "Tracy Duncan"
  content_markdown = ""
  image = "/uploads/tracy2020.jpg"
  caption = ""
  link_text_markdown = "Meet&nbsp;Tracy"
  link_url = "/tracy-duncan/"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "Felicia Minto"
  content = ""
  image = "/uploads/2022_Felicia_pic.png"
  caption = ""
  link_text_markdown = "Meet&nbsp;Felicia"
  link_url = "/felicia-minto/"
  open_in_new_tab = false

[[page_sections]]
headline = "Credit Union Background"
content_markdown = """
On January 26, 1954, we received our charter as North Montana Texaco Employees Federal Credit Union. In 1958 our name was changed to Puget Sound Texaco Federal Credit Union. In June of 1988, Seattle Texaco Credit Union merged with our credit union. When Texaco merged with Equilon, the new credit union name became E. E. Federal Credit Union. In 2002, the refinery changed to Shell, and the credit union name had to change again. Puget Sound Refinery Federal Credit Union was chosen, hoping it wouldn’t have to be changed again.

For all the changes in the history of our credit union, not a lot has changed. We are still “people helping people.” Our goal is to make sound loans to our membership while paying a reasonable dividend rate on their shares.

**Current HF Sinclair PSR employees; retired pensioners or annuitants, long-term contractors, All USW Local 12-591 members, and immediate family members of those listed, as well as spouses of persons who died while within the field of membership of this credit union, are eligible to join.**

*Not for Profit, Not for Charity, But for Service.*"""
num_col_sm = "2"
num_col_md = "2"
num_col_lg = "2"
num_col_xl = "2"
backtotop = true
block = "multi-column"

[[page_sections]]
heading_markdown = "[Click here to Meet our Board Members](/board-members)"
background_choice = "psrfcu"
text_choice = "white"
block = "hero"
+++
