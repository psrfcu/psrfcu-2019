+++
date = "2019-09-03T18:05:59+00:00"
featured_image = "/uploads/calendars website.jpg"
title = "Calendar photos wanted"
type = "news"
+++

Work has begun on the 2020 PSRFCU wall calendar, and we want your photos of Anacortes and the surrounding area.

Please submit photos in jpeg or gif format to info@psrfcu.org .

All photo submissions that are selected become property of PSRFCU and may be used for promotion or advertising.