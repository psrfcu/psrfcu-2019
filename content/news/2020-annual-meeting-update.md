+++
date = 2020-03-18T16:08:03Z
featured_image = "/uploads/earthmeeting.jpg"
title = "2020 Annual Meeting update!"
type = "news"
+++
Please note:  The 2020 Annual Meeting will be held VIRTUALLY via Zoom on May 21, 2020 at 6 pm.  
A link will be sent to your email on file, so please make sure it is up to date with us.

You can simply send us an email at info@psrfcu.org and let us know you have an email address update.