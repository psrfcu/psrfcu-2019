+++
date = 2020-05-15T15:42:24Z
featured_image = "/uploads/annualmeeting.jpg"
title = "2020 Annual Meeting"
type = "news"
+++

Thank you to everyone that attended the 2020 Annual Meeting via Zoom, we had a great turnout!  

_Please register for the 2020 Annual Meeting.  We will be holding the meeting virtually and hope you will all join us!_

_Register now and receive a fun gift bag that includes lip balm, a post-it pad, mug, face mask, hand sanitizer and more._

![](/uploads/prizes-1.jpg)