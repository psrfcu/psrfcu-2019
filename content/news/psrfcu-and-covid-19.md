+++
date = 2020-03-18T07:00:00Z
featured_image = "/uploads/hands-3.jpg"
title = "PSRFCU and Covid-19"
type = "news"
+++

Community transmission of Covid-19 is occurring in Skagit County. Now is the time to follow Health Officer Recommendations and protect our community.

The credit union will remain OPEN, but we are limiting contact for the safety of our membership and employees.

Need a check? Please call the office, (360) 293-4862, to make an appointment if you require an in-person meeting, or we can have your check ready to slip through our "drive thru" door slot. Need to make a deposit?  Fill out an envelope, supplied in the mailbox to the right of our door, and slip it through the mail slot.  We can email or mail you a receipt. If you require a receipt immediately, call or text us at (360) 610-7140.  (Please feel free to take the pen with you so we don't spread germs!)

You can sign up for online banking, apply for loans, request a check or transfer money online.  We have updated our services to include secure online document signatures so you can sign your documents without leaving home.  We are also offering phone banking at 1-800-860-5704, you will need the PIN 948 and your 4 digit account number.

Please use the links below and if you have any questions feel free to call or email us at info@psrfcu.org .

* Sign up online to access all of your account needs:  [Access your accounts](https://obc.itsme247.com/948/)
* Apply for auto, personal or HELOC loans: [ Apply for a loan](https://obc.itsme247.com/948/)
* Apply for a mortgage loan:  [Apply for a mortgage](https://www.consolidatedccu.com/mortgage/mortgage.html?a=PUGETSOUNDREFINERYFCU)
* Questions about our current account services:  [Current account services](https://psrfcu.org/account-services/)
* Important updates regarding PSRFCU and our members:  [Important news and updates](https://psrfcu.org/news/)