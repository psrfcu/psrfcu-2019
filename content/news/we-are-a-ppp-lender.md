+++
date = 2020-04-16T18:33:25Z
featured_image = "/uploads/smallbusinessPPP.jpg"
title = "Did you know we are a PPP Lender?"
type = "news"
+++

\***PPP applications are being accepted through August 8, 2020.**

**HOWEVER, this is a first come first serve basis and we will need to move FAST to get your application in.**

In response to the small business funding gap that our members have brought to our attention we have moved mountains to become a PPP (Paycheck Protection Program) Lender!

Did you know that any of our members who meet the SBA criteria can qualify for a forgivable loan? See if you qualify [**here**](/uploads/CARESActVisual.pdf)**.** Do you have family members who own a small business? They can [**become a member**](https://forms.joinmycu.com/mop/948/newmember/promotional) at PSRFCU and apply for a PPP loan also.

Once you are a member, gather the [**needed information**](/uploads/SBAPaycheckProtectionProgramFinancingChecklist.pdf) and fill out the[ **2020 2483 Borrower Application revised 6/12/2020**](/uploads/PPPBorrowerApplicationFormRevisedJune122020Fillable508.pdf). Submit all of your documents by [**secure upload here**](https://forms.joinmycu.com/mumikpgfpq/cuwebsite/standalone). If you need to drop them off to the office, please call for an appointment. If you are **self employed with no employees** you can find important information or information needed [**here.**](/uploads/PPPScheduleCOnlyChecklist.pdf)

\*The SBA site is bogged down. We are aware of this, and we will continue to keep trying for every application we get! Please still send in your application as our pipeline of applications is low. There could also be another round of funding once these dollars are gone.

\*Also note that there is a floor minimum of $1,000 via the Connect.SBA site. We have had some success directly on the ETRANs site at doing these lower dollar amount PPP loans if needed.
