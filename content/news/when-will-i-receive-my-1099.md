+++
date = 2019-12-09T14:32:53Z
featured_image = "/uploads/taxes.jpg"
title = "When will I receive my 1099?"
type = "news"
+++

#### **You will receive your 1099 on your year end December 31 statement and you will be able to access it the first week of January via online banking/e-statements.**