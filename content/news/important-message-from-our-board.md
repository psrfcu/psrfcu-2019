+++
date = 2020-03-12T04:26:33Z
featured_image = "/uploads/psrfcu-logo.png"
title = "Important Message from our Board"
type = "news"
+++

Dear Members,

As you may have seen Shell PSR recently officially posted they are up for sale. While we are a closed membership federal credit union that services Shell PSR and their long term contractors, we are not owned or operated by Shell PSR. The selling of Shell PSR will not affect us as your local friendly faced credit union. 

Prior boards have gone through a number of name changes due to the sale of the refinery, and had the forethought to update our name to Puget Sound Refinery FCU, so that any brand changes outside of our control, like the selling of the refinery, would be weathered smoothly. 

We appreciate you, and look forward to continuing providing stellar service for you and your family for years to come. 

Sincerely, The PSRFCU Board of Directors 