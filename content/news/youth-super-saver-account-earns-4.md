+++
date = 2020-09-04T16:23:40.000Z
title = "Youth Super Saver Account Earns 4%!"
type = "news"
featured_image = "/uploads/psrfcu-flyer-5.jpg"
lastmod = "2022-01-20T20:10:34.300Z"
+++

Earn as much as 4% on your Youth Super Saver Account with only $500.  Sign up now!  
You'll get a free child's mask if you open a Youth Super Saver account with us between now and October 31, 2020! 
Find out more information by emailing us at info@psrfcu.org.
