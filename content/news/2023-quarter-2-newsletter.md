+++
title = "2023 Quarter 2 Newsletter"
date = 2023-04-01T00:05:00.000Z
featured_image = "/uploads/2397_rdc_instagram_1080x1080.png"
hideImage = false
draft = false
type = "news"
lastmod = "2023-02-01T21:21:42.546Z"
+++

See the [2023 Quarter 2 Newsletter](https://conta.cc/3nMWddX) for Quarter 2 Giveaway, annual meeting, annual scholarship, and more!