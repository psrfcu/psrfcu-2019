+++
title = "2022 Quarter 3 Newsletter"
date = 2022-07-05T00:05:00.000Z
featured_image = "/uploads/applewatch.jpg"
hideImage = false
draft = false
type = "news"
lastmod = "2022-08-16T16:54:52.722Z"
+++

See the [2022 Quarter 3 Newsletter](https://conta.cc/3SO9cHi) to learn how to win an Apple Watch, member appreciation lunch, activate your debit card, and more!
