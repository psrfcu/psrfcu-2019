+++
title = "2023 Quarter 1 Newsletter"
date = 2023-02-01T00:05:00.000Z
featured_image = "/uploads/2396_estatement_instagram_JPG.jpg"
hideImage = false
draft = false
type = "news"
lastmod = "2023-02-01T21:21:42.546Z"
+++

See the [2023 Quarter 1 Newsletter](https://conta.cc/3I666Mb) Quarter 1 Giveaway, how to access your online e-statements and 1099-INT, our dividend rate increases and more!