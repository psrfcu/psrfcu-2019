+++
title = "Membership and Account Agreement updates. "
date = 2021-10-28T00:05:00.000Z
featured_image = "/uploads/update.jpeg"
hideImage = false
draft = false
type = "news"
+++

Effective 10/28/2021, the Credit Union has updated its Membership and Account Agreement. Please review the <a target="_blank" rel="noopener" href="/uploads/Membership-and-Account-Agreement---FINAL.pdf"><strong><em>Membership and Account Agreement</em></strong></a>.
