+++
date = "2019-08-12T09:40:00-07:00"
featured_image = "/uploads/pork-1.jpg"
title = "Member Appreciation Lunch"
type = "news"
+++

Thank you to everyone that attended, we had a great lunch even with the rain!

Our 2019 Member Appreciation Lunch is Wednesday, August 21st, from 11am-2pm at the credit union.

We'll be serving up pulled pork sandwiches and sides, at our picnic style lunch.

Bring the whole family!