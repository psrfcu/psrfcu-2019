+++
date = 2020-04-16T15:13:17Z
featured_image = "/uploads/taxreturn.png"
title = "Waiting on your tax refund?"
type = "news"
+++

Sign up to receive an email notification when your tax refund is deposited into your account!  Simply [**log in**](https://obc.itsme247.com/948/), click on 'Info Center', select 'eAlert Subscriptions', and then 'Create New ACH Deposit or Withdrawal eAlert Subscription'.