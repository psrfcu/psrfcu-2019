+++
title = "2025 Quarter 1 Newsletter"
date = 2025-01-13T00:05:00.000Z
featured_image = "ice-crystal-64157_1920 (1).jpg"
hideImage = false
draft = false
type = "news"
lastmod = "2025-01-13T22:10:51.981Z"
+++

See the [2025 Quarter 1 Newsletter](https://lp.constantcontactpages.com/cu/rsbtuDQ/psrfcu2025q1newsletter
) to learn about our extension on our 70th Anniversary Loan Special, how to access your End-of-Year statements and 1099 for taxes, and current rates on savings and CDs to earn more on your money!