+++
date = 2019-07-22T19:51:02.000Z
featured_image = "/uploads/annualmtg.jpg"
title = "2019 Annual Meeting"
type = "news"
+++

PSRFCU recently hosted an amazing annual meeting, with almost 90 members and family in attendance it was a huge success\!

Thank you to everyone that was able to make it for tacos and prizes and thank you to our board for presenting all of the 2018 information.

Welcome new board member, Dean Iverson\!
