+++
title = "Here's the BIG NEWS from the CARES ACT revision"
date = 2021-03-12T18:25:20Z
featured_image = "/uploads/shutterstock_1729384105.jpg"
hideImage = false
type = "news"
+++

**Here's the BIG NEWS from the CARES ACT revision about PPP loans!**

1\. There is $285 Billion available for PPP Loans (Draw One & Draw Two), _BUT_ it’s on a first-come-first-serve basis. When it’s gone, it’s gone.

2\. YOUR business is most likely **eligible** for the **Draw Two SBA PPP** loan if you had any quarter of gross revenue in 2020 at least 25% less than the same quarter in 2019.

1. _The easiest way for you to see if you can get the **same amount (or more) of Draw Two money** you received for Draw One is_ to get out your State B&O/Sales Tax reports -

   (WA State Department of Revenue) both years and use the attached spreadsheet to confirm you qualify.
2. The rest is easy because PSRFCU is your SBA PPP Lender.

3\. _BOTH DRAW ONE and DRAW TWO ARE FORGIVABLE and NON-TAXABLE LOANS!!!_

_If you know someone who didn’t apply for the SBA PPP Draw One Loan and you think they might qualify, have them call PSRFCU at 360-293-4862 ASAP and ask for Darene. We can submit Draw One SBA PPP loans for businesses who qualify._

_PSRFCU will do everything we can to help them get some of this business-saving money to help our entire business community recover more quickly._

**_Please let Darene know ASAP (by_** **_email_** **_or voice mail) if you want to apply for DRAW TWO_**_._

[PPP Second Draw Application Form](/uploads/2021-01-ppp-second-draw-borrower-application-form.pdf)