+++
date = 2020-02-03T14:20:32Z
featured_image = "/uploads/turbotax2020.jpg"
title = "Members save up to $15 with Turbo Tax!"
type = "news"
+++

Check out the latest from Love My Credit Union where you can [save up to $15 on Turbo Tax and get your maximum refund.](http://links.lovemycreditunion.org/client/love_my_cu/banner/?bid=254&campid=23&clientid=9145&sid=1)

Tax filing has been extended to July 15, 2020!