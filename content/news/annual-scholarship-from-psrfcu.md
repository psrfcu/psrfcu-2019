+++
date = 2020-05-24T22:44:13.000Z
featured_image = "/uploads/graduation-cap.jpg"
title = "Annual Scholarship from PSRFCU "
type = "news"
draft = true
lastmod = "2024-04-26T18:54:58.825Z"
+++

We have two $500 scholarships up for grabs, get your submission in today for a chance to receive one of them.

## Eligibility

Graduating High School Seniors from Skagit, Whatcom, Snohomish, Island Counties, Members in Good Standing from PSRFCU, sons and daughters of members in good standing; who have applied to or have been accepted to an accredited institution.

## Scholarship Dates

Applications received: May 22, 2020 – July 15, 2020

## Qualifications

A 2-3 page, double spaced essay, regarding:

Corona Virus/COVID-19 and how it has affected our local credit union.  
The affects of the Corona virus on credit unions in general.  
What things have changed and what things have stayed the same.  
How and why you will use the credit union.

## Please Include

Name, address, phone, email, current transcripts and how long you have been a member, 2 letters of recommendation (non-family) with examples of teamwork, reliability and accountability.

## Tips

Answer all questions, research the subject and provide facts, (please do not only use Wikipedia as it may contain undocumented facts); spelling, grammar and punctuation will be evaluated.

[Printable scholarship information here.](/uploads/annualscholarship2020.pdf)
