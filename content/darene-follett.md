+++
date = "2019-07-29T00:00:00-07:00"
description = "Meet our employee, Darene Follett."
layout = "blocks"
title = "Darene Follett"
[[page_sections]]
background_choice = "whiteish"
background_image = ""
block = "feature"
content_markdown = "Darene Follett is a Certified Public Accountant (CPA) and is the current CEO of PSRFCU.  She has a BA from WSU in Business Management with a focus in accounting and professional writing.  She came to us with a strong background in management and audit & nonprofit accounting, working as a manager for Safeway for over 17 years and then public accounting as a Senior Auditor/Business Tax professional prior to starting at PSRFCU in 2015.\n\nShe has lived in Anacortes for almost 12 years with her husband and two boys, and her family has lived on the south side of Whidbey for over 50 years.  Her husband works as a boat builder in La Conner and they, as a family, are committed to helping the local community by volunteering and helping guide many local nonprofits within Skagit Valley where they sit on a number of boards.  \n\nDarene is committed to helping our members with any financial questions they have, and making sure the credit union is fiscally responsible in their lending and savings practices.  Since coming aboard we have seen many positive changes to the credit union and look forward to what the future holds!"
headline = "Darene Follett"
media_alignment = "Left"
showmobile = true

[page_sections.cta]
enabled = false
link_text_markdown = ""
link_url = ""
open_in_new_tab = false
[page_sections.media]
alt_text = ""
image = "/uploads/Darene 2019b.jpg"
+++
