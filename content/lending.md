+++
aliases = [ "/services/lending-services/", "/rates/" ]
date = 2019-06-05T05:00:00.000Z
description = "Search our lending services for home loans, HELOC info, auto and personal loans or apply for a credit card.  View our loan rates, loan terms and credit tiers."
featured_image = ""
layout = "blocks"
title = "Lending Services"
lastmod = "2022-06-29T05:55:46.481Z"

[menu.main]
name = "Lending"
open_in_new_tab = false
pre = ""
url = "/lending/"
weight = 3

[[page_sections]]
headline = "Lending Services"
content_markdown = """
Puget Sound Refinery Federal Credit Union offers a wide variety of loans at affordable rates. Are you looking to buy your first car, a new car or pickup? Any member having at least one share may apply.

You can expect all of our lending services to come with the prompt and friendly service you deserve as a member of Puget Sound Refinery Federal Credit Union. We think our combination of outstanding service, great rates and convenience just can’t be beat!"""
num_col_lg = ""
num_col_md = ""
num_col_sm = ""
num_col_xl = ""
backtotop = false
block = "multi-column"

[[page_sections]]
headline = ""
background_choice = "whiteish"
backtotop = false
block = "cta-banner"

  [[page_sections.ctas]]
  headline = "Home&nbsp;Loan"
  content = ""
  image = "/uploads/home-loan.png"
  caption = ""
  link_text_markdown = "More&nbsp;Info"
  link_url = "#home-loans"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "HELOC"
  content = ""
  image = "/uploads/loan.png"
  caption = ""
  link_text_markdown = "More&nbsp;Info"
  link_url = "#heloc"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "Auto&nbsp;Loan"
  content = ""
  image = "/uploads/autoloan.png"
  caption = ""
  link_text_markdown = "More&nbsp;Info"
  link_url = "#auto-loans"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "Personal&nbsp;Loan"
  content = ""
  image = "/uploads/loancontract.png"
  caption = ""
  link_text_markdown = "More&nbsp;Info"
  link_url = "#personal-loans"
  open_in_new_tab = false

  [[page_sections.ctas]]
  headline = "Credit&nbsp;Card"
  content = ""
  image = "/uploads/credit-card.png"
  caption = ""
  link_text_markdown = "More&nbsp;Info"
  link_url = "#credit-card"
  open_in_new_tab = false

[[page_sections]]
headline = "Home Loans"
content_markdown = """
You have been asking us to do home loans, and we have found a way to provide low cost home loans with excellent [rates](https://www.consolidatedccu.com/mortgage) to you by partnering with Consolidated Community Credit Union. This means the loans will not be on our books, but you will be working with us through the 1st steps.
Consolidated Community Credit Union [current rates](https://www.consolidatedccu.com/mortgage)."""

background_choice = "whiteish"
background_image = ""
media_alignment = "Right"
backtotop = false
showmobile = false
block = "feature"

  [page_sections.cta]
  enabled = true
  link_text_markdown = "Apply"
  link_url = "https://www.consolidatedccu.com/mortgage/mortgage.html?a=PUGETSOUNDREFINERYFCU"
  open_in_new_tab = true

  [page_sections.media]
  alt_text = "Nice lit up home at dusk"
  image = "/uploads/shutterstock_1109504687.jpg"

[[page_sections]]
headline = "HELOC"
content_markdown = """
We have begun offering **Home Equity Lines of Credit** (HELOC)! Our HELOCs will be in the house, offering you hassle-free, low fee options, that we think is worth taking advantage of. Please see more information about our HELOC Loan Rates below.

To learn about our Subordination Agreement requirements, contact the office!"""

background_choice = "whiteish"
background_image = ""
media_alignment = "Right"
backtotop = false
showmobile = false
block = "feature"

  [page_sections.cta]
  enabled = true
  link_text_markdown = "Apply"
  link_url = "https://obc.itsme247.com/948/"
  open_in_new_tab = true

  [page_sections.media]
  alt_text = "Replacing flooring with wood planks"
  image = "/uploads/shutterstock_184931924.jpg"

[[page_sections]]
heading = "Credit Tiers"
url = "https://docs.google.com/spreadsheets/d/1lLYp32-F09DyKNCyjTdgu1EWFPI1dASHV16sXkOMyDU/export?format=csv"
description_markdown = ""
footnotes_markdown = ""
block = "table-csv"

[[page_sections]]
heading = "HELOC Loan Rates"
url = "https://docs.google.com/spreadsheets/d/1cmuk4nL0tygB1j4nUAmZMMyQlUjLAV4q_9ldK2VOPyQ/export?format=csv"
description_markdown = """The Prime Rate was {{< prime "rate" >}} as of {{< prime "date" >}}. All rates are APR and subject to change without notice."""
block = "table-csv"

[[page_sections]]
heading = "HELOC Terms"
url = "https://docs.google.com/spreadsheets/d/1kTJqaZMd41nS9cEHjo7UsT2ijho33HcfH-RL68Gw-bo/export?format=csv"
description_markdown = """
Rates shown for VPR (Variable Periodic Rate) are as published in the Wall Street Journal plus or minus margin. VPR is subject to change monthly.  Check the current prime rate [**here**](https://www.wsj.com/market-data/bonds/moneyrates).

* All loans are set up for a 15-year repayment at each draft like a LOC. You can set it up to have a minimum interest-only payment or payment and interest to pay off over 15 years. If interest only is chosen, at the end of 15 years it becomes a closed-end loan and you have 15 years to repay the total balance and interest.
* A minimum payment of $100.00/mo is required regardless of the amount drawn from the line of credit.
* Debt to income must be 45% or below. Over $90,000 requested is subject to board approval. Any first placed mortgages over $500,000 subject to other rates/restrictions/approvals.
* If any loans with PSRFCU are delinquent, withdrawal advances will be frozen until all accounts are current. Reconveyance fee is $450 (sometimes varies depending on which Title Company processes it; call for more info) of passthrough cost. Cost of application for HELOC is $750.

**Late Fee**

There will be a $25.00 late fee if the payment is received more than 15 days past the due date."""
footnotes_markdown = ""
block = "table-csv"

[[page_sections]]
block = "backtotop"
width = 4

[[page_sections]]
headline = "Auto Loans"
background_choice = "whiteish"
content_markdown = "Our auto loans offer you hassle free, low fee options at great rates.  [Calculate](/resources/) your auto payment."
background_image = ""
media_alignment = "Right"
backtotop = false
showmobile = false
block = "feature"

  [page_sections.cta]
  enabled = true
  link_text_markdown = "Apply"
  link_url = "https://obc.itsme247.com/948/"
  open_in_new_tab = true

  [page_sections.media]
  alt_text = ""
  image = "/uploads/shutterstock_298063157.jpg"

[[page_sections]]
heading = "Credit Tiers"
url = "https://docs.google.com/spreadsheets/d/1umVO6Fdye0EnR9Xh8zKj0P24z-O8qMmHCw5iGPfhDs8/export?format=csv"
description_markdown = ""
footnotes_markdown = ""
block = "table-csv"

[[page_sections]]
heading = "Auto Loan Rates"
url = "https://docs.google.com/spreadsheets/d/1I-F-tps4zgVNVO8lTyaf7_d8AN3Iv1nRU-RZD4uCqvs/export?format=csv"
description_markdown = "Rates shown are floor rates with payroll direct deposit requirement. Starting at a 60 month term that increases 0.50% for each year added with the exception of the A++ tier."
footnotes_markdown = ""
block = "table-csv"

[[page_sections]]
heading = "Auto Loan Terms"
url = "https://docs.google.com/spreadsheets/d/1ILwK5d9U8BjEeNKK7FIZtr-xitigqlObf_6o_3BgbYk/export?format=csv"
description_markdown = """
**_ALL RATES ARE DETERMINED BY YOUR CREDIT SCORE From 2.75%-18.00%_**

Members receive 0.50% off rates when paying by payroll deduction. You can receive a 0.25% rate reduction for paying by ACH or automatic transfer.

The maximum amount financed will be 95% of the purchase price, not to exceed 95% of the JD Power High Book Value. A tiered credit can finance 100% of the purchase of a vehicle. A+ tiered credit can finance 100%+++ (tax, licensing, fees up to 120% of JD Power). Add 0.50% for each year over 60 months. Add 0.50% if over 100,000 miles or aged over 10 years. A++ tier begins to increase at 60 months @ 0.25% up to 84 months, and then 0.50% after 84 months.

**Down Payment Requirements**

Persons with a “D” credit rating may either be required to pay a $500 Member Assist Auto Program (MAAP) fee or be required to have a co-signer or be required to do both."""
footnotes_markdown = """
**Insurance**

Proof of full coverage insurance is required on all collateral loans (comprehensive and collision).

**Late Fee**

A late fee of $25.00 will be assessed if your payment is received more than 15 days past the due date.

We offer: **GAP Insurance**, **Mechanical Breakdown Protection**"""
block = "table-csv"

[[page_sections]]
block = "backtotop"
width = 4

[[page_sections]]
headline = "Personal Loans"
content_markdown = """
Take out a Signature or Share Secured loan to cover medical expenses, vacation, holidays, etc.

Signature loans up to $10,000.  Share Secured loans do not have a set maximum.  """
background_choice = "whiteish"
background_image = ""
backtotop = false
media_alignment = "Right"
showmobile = false
block = "feature"

  [page_sections.cta]
  enabled = true
  link_text_markdown = "Apply Now"
  link_url = "https://obc.itsme247.com/948/"
  open_in_new_tab = true

  [page_sections.media]
  alt_text = "Harbour and boats on sunny day at Boka Kotor Bay, Montenegro, Europe"
  image = "/uploads/shutterstock_259595084.jpg"

[[page_sections]]
heading = "Credit Tiers"
url = "https://docs.google.com/spreadsheets/d/1umVO6Fdye0EnR9Xh8zKj0P24z-O8qMmHCw5iGPfhDs8/export?format=csv"
description_markdown = ""
footnotes_markdown = ""
block = "table-csv"

[[page_sections]]
heading = "Personal Loan Rates"
url = "https://docs.google.com/spreadsheets/d/1X46GPKjLhbKfCJ3BXHXxXq8xe8fESneaSOdK-7U2ZMo/export?format=csv"
description_markdown = "Rates shown are floor rates with payroll direct deposit requirement. Starting at a 60-month term that increases 0.50% for each year added except for the A++ tier."
footnotes_markdown = """
Share Secured loan rate is 4.0% above the current dividend rate with automatic payment.  Click below to get a fillable PDF application and then use the [Upload](https://forms.joinmycu.com/mumikpgfpq/cuwebsite/standalone) feature through this site to send it to us.

[Shared Secured Loan Application](/uploads/shared-secured-loan-form-app-interactive.pdf)"""
block = "table-csv"

[[page_sections]]
block = "backtotop"
width = 4

[[page_sections]]
headline = "Credit Card"
content_markdown = "We are now proudly partnering with LSC, a Credit Union Service Organization (CUSO), to offer you an opportunity to apply for a VISA Credit Card! When filling out the application, you must ensure to use the drop down to click on our name, **Puget Sound Refinery FCU**."
background_choice = "whiteish"
background_image = ""
media_alignment = "Right"
backtotop = false
showmobile = false
block = "feature"

  [page_sections.cta]
  enabled = true
  link_text_markdown = "Apply"
  link_url = "https://www.mycucard.com/"
  open_in_new_tab = true

  [page_sections.media]
  alt_text = ""
  image = "/uploads/shutterstock_336271295.jpg"

[[page_sections]]
block = "backtotop"
width = 4
+++
