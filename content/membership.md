+++
date = 2019-06-04T05:00:00.000Z
description = "See who can join, what you need to open your account and view other services we offer."
featured_image = ""
layout = "blocks"
title = "Membership"
lastmod = "2022-06-06T21:03:47.913Z"

[menu.main]
open_in_new_tab = false
pre = ""
weight = 1

[[page_sections]]
backtotop = false
block = "multi-column"
content_markdown = "Find out who is eligible to be a PSRFCU member, and check out our member benefits, including many Love My Credit Union discounts."
headline = "Membership and Services"
num_col_lg = "1"
num_col_md = "1"
num_col_sm = "1"
num_col_xl = "1"

[[page_sections]]
background_choice = "whiteish"
backtotop = false
block = "cta-banner"
headline = ""

  [[page_sections.ctas]]
  caption = ""
  content_markdown = ""
  headline = "Who Can Join?"
  image = "/uploads/group-chat.png"
  link_text_markdown = "More Information"
  link_url = "#who-can-join"
  open_in_new_tab = false

  [[page_sections.ctas]]
  caption = ""
  content_markdown = ""
  headline = "Member Benefits & Discounts"
  image = "/uploads/save-money.png"
  link_text_markdown = "More Information"
  link_url = "#member-benefits-discounts"
  open_in_new_tab = false

  [[page_sections.ctas]]
  caption = ""
  content_markdown = ""
  headline = "Apply For Membership"
  image = "/uploads/web.png"
  link_text_markdown = "Apply Now"
  link_url = "https://forms.joinmycu.com/mop/948/newmember/promotional"
  open_in_new_tab = false

[[page_sections]]
background_choice = "psrfcu"
block = "hero"
heading_markdown = "Who Can Join?"
text_choice = "white"

[[page_sections]]
backtotop = true
block = "multi-column"
content_markdown = """
**Current HF Sinclair PSR Employees, Retired Pensioners or Annuitants, Long-term Contractors, USW Local 12-591 members, and Immediate Family Members of those listed, as well as Spouses of persons who died while within the field of membership of this credit union, are eligible to join. Click** [**HERE**](https://forms.joinmycu.com/mop/948/newmember/promotional) **to join\\!**

![open to all us 12-591 members and their immediate family](https://psrfcu.imgix.net/uploads/open-to-all-members.png?w=560&amp;auto=format "OPEN TO ALL US 12-591")

<div class="text-center font-bold text-lg my-4 print:hidden">Click below to get started!<a href="https://obc.itsme247.com/948/"><img class="block mx-auto px-4 py-2 rounded border-2 border-psrfcu" title="Membership Application Image Link" width="142px" src="/uploads/logo_im247.gif" /></a></div>

When you log in to "It's Me 24/7" for the first time your default log-in will be your 4 digit account number and your password is the last 4 digits of your social security number and your 4 digit birth year (eg. 55551975).

To complete your membership, we will need a copy of your ID, your signature card signed, and $50. For minors to join we need a copy of their social security card and birth certificate, and $5. They will need a joint member over 18 years of age.

Members must make at least one transaction in every 18 month period or they are considered dormant. Being dormant is having no contact with the credit union or response to our attempts to contact you, we will begin to charge the account a $5.00 per month dormant fee until by law the account is sent to the state for escheatment purposes after three years, or there are no longer funds available. At this point, the account will be closed on the member’s behalf.

**New Member Incentive\\!** You can earn $25 for every eligible new member you bring us\\! Bring in a new member who obtains a collateralized loan through us within 30 days, and you both be eligible to receive $50 deposited into your Share savings account\\! Ask us how today\\!"""
headline = ""
num_col_lg = "2"
num_col_md = "2"
num_col_sm = "1"
num_col_xl = "2"

[[page_sections]]
background_choice = "psrfcu"
block = "hero"
heading_markdown = "Member Benefits & Discounts"
text_choice = "white"

[[page_sections]]
backtotop = true
block = "multi-column"
content_markdown = """
* [**Rewards Program** ![](/uploads/LMCUR_heart_lockups_whitebg_Horizontal_2.jpg)](http://links.lovemycreditunion.org/client/love_my_cu/banner/?bid=5306&campid=36&clientid=9145&sid=1)
PSRFCU participates in the [Love My Credit Union Rewards Program](http://links.lovemycreditunion.org/client/love_my_cu/banner/?bid=5306&campid=36&clientid=9145&sid=1).
Visit our link to their page to see all currently available offers
and discounts.

* [**Vacation Packages** ![](/uploads/gat-logo-website.jpg)](https://www.getawaytoday.com/?referrerid=535)

* [**Disney Discounts!**](https://www.getawaytoday.com/tickets/disneylandresortarea?referrerid=535)"""
headline = ""
num_col_lg = "1"
num_col_md = "1"
num_col_sm = "1"
num_col_xl = "1"
+++
